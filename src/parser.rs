use irc::client::prelude::*;
use crate::ProcessedMessage;





pub fn parse(message: Message)-> ProcessedMessage{
    /* Example Messages
    :GRLaken!68e1b3a9@gateway/web/cgi-irc/kiwiirc.com/ip.104.225.179.169 PRIVMSG #GRLaken xchfj\r\n
    */

    let mut message_proc =  message.to_string();
    //print!("{}", message_proc);

    //message_proc= message_proc.strip_suffix("\r\n");

    if message_proc.ends_with('\n') {
        message_proc.pop();
        if message_proc.ends_with('\r') {
            message_proc.pop();
        }
    }


    let mut y: Vec<&str> = message_proc.split(" ").collect();
    //let v: Vec<&str> = "abc1defXghi".split(|c| c == '1' || c == 'X').collect();

    let cmd= y[1];
    y.remove(1);
    let src= y[0];
    y.remove(0);



    let mut items = Vec::<String>::new();
    for item in &y {
        items.push(item.to_string());
    }



    //Finish this up!!
    let x = ProcessedMessage{
        source: src.to_string(),
        command: cmd.to_string(),

        command_data: items,
    };


    x
}
