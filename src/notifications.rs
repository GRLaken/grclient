use notify_rust;

pub fn notify(msg: String)-> std::result::Result<(), notify_rust::error::Error>{
    notify_rust::Notification::new()
        .summary("GRClient")
        .body(&msg)
        .show()?;
    Ok(())
}
