use futures::*;
use irc::client::prelude::*;

mod notifications;
use notifications::*;

mod parser;
use parser::*;

#[tokio::main]
async fn main() -> Result<(), irc::error::Error> {
    // We can also load the Config at runtime via Config::load("path/to/config.toml")
    //let config = Config::load("path/to/config.toml")
    let config = Config {
        nickname: Some("GRClient".to_owned()),
        server: Some("chat.freenode.net".to_owned()),
        channels: vec!["#grclient".to_string()],
        ..Config::default()
    };

    let mut client = Client::from_config(config).await?;
    client.identify()?;

    //let _response = notify("Hi there".to_string());

    let mut stream = client.stream()?;

    while let Some(message) = stream.next().await.transpose()? {
        //println!("{:?}", message);

        // Only works after word processing
        //let mut proc_message = message.to_string();
        let x = parse(message);
        //println!("{:?}", x);

        let command_pre: String = x.command;
        let command: &str = &command_pre[..];  // take a full slice of the string


        match command {
            "PRIVMSG" => {
                // iterate through and see which sections start with :
                let mut y= String::new();
                for z in &x.command_data[1..]{
                    y.push_str(&format!("{} ", z));
                }

                println!("{} {} | {:?}", command, x.command_data[0], y);
            },
            _ => {}
        }
    }
    Ok(())
}

#[derive(Debug)]
pub struct ProcessedMessage{
    pub source: String,
    pub command: String,
    pub command_data: Vec<String>,
}
